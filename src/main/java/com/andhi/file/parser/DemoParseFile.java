/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andhi.file.parser;

import com.andhi.file.helper.FileImporter;
import com.andhi.file.helper.Person;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class DemoParseFile {
    public static void main(String[] args) throws IOException{
        FileImporter fi = new FileImporter("src/main/resource/daftar-nasabah.txt");
        List<Person> hasilImport = fi.proses();
        System.out.println(hasilImport.size() +" data berhasil diimport dari file");
        
        for(Person a:hasilImport){
            System.out.println("Nomor : "+a.getNomor());
            System.out.println("Nama : "+a.getNama());
            System.out.println("Email : "+a.getEmail());
        }
    }
}
