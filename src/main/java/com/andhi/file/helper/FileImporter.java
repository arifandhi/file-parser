/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andhi.file.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class FileImporter {

    private BufferedReader fileReader;
    private File fileInput;

    public FileImporter(String namaFile) {
        this.fileInput = new File(namaFile);
    }
//     throws FileNotFoundException

    private void bukaFile() throws FileNotFoundException {
        try {
     
            fileReader = new BufferedReader(new FileReader(fileInput));
        } catch (FileNotFoundException ex) {
            System.out.println("FILE NOT FOUND");
        }
    }

    public void tutupFile() throws IOException {
        if (fileReader != null) {
            fileReader.close();
        }
    }

    public List<Person> proses() throws IOException {
        bukaFile();

        String data = fileReader.readLine();
        System.out.println("Header : " + data);

        List<Person> hasil = new ArrayList<Person>();
        int urutan=1;
        while (data != null) {
            data = fileReader.readLine();
            if (data != null) {
                System.out.println("Data : " + data);

                String[] isiVariabel = data.split(",");
                if (isiVariabel.length != 3) {
                    throw new IllegalStateException("Format data salah, harusnya ada 3 data");
                }

                Person n = new Person();
                n.setNomor(isiVariabel[0]);
                n.setNama(isiVariabel[1]);
                n.setEmail(isiVariabel[2]);
                n.setUrutan(urutan);
                urutan++;
                hasil.add(n);
            }

        }
        tutupFile();
        return hasil;
    }
}
